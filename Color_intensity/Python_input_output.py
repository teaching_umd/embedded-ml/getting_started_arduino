# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 12:17:16 2021

@author: sshah389
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 17:38:35 2021

@author: sshah389
"""

import serial
import numpy as np
from matplotlib import pyplot 
import time
i=0
N=100
red_out=np.zeros([N])
green_out=np.zeros([N])
blue_out=np.zeros([N])
int_out=np.zeros([N])
ser = serial.Serial('COM8', 9600, timeout=1) ##change this to your COMport

def byte_to_int(byte_value):
    out_string = byte_value.decode()
    out_string1 = out_string.strip()
    out_int = int(out_string1)
    return out_int
    


while i<N:
    red_out[i]=byte_to_int(ser.readline())
    green_out[i]=byte_to_int(ser.readline())
    blue_out[i]=byte_to_int(ser.readline())
    int_out[i]=byte_to_int(ser.readline())
    i=i+1
   

pyplot.plot(red_out[1:N])  
pyplot.show() 
pyplot.plot(green_out[1:N])  
pyplot.show()  
pyplot.plot(blue_out[1:N])  
pyplot.show() 
pyplot.plot(int_out[1:N])  
pyplot.show() 
ser.close()
