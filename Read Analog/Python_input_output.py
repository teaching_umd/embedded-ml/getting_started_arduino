# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 12:17:16 2021

@author: sshah389
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 17:38:35 2021

@author: sshah389
"""

import serial
import numpy as np
from matplotlib import pyplot 
import time

i=0
N=100
output=np.zeros([N])
ser = serial.Serial('COM8', 9600, timeout=1) ##change this to your COMport
while i<N:
    out_byte =  ser.readline()
    out_string = out_byte.decode()
    out_string1 = out_string.strip()
    out_int = float(out_string1)
    output[i]=out_int
    time.sleep(0.1)
    i=i+1
    
pyplot.plot(output[5:100])    
ser.close()
