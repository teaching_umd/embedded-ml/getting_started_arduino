This repo contains folders with arduino.INO files and Python scripts. These files perform different functions as described in the Getting_started document. Lecture 3 (https://www.youtube.com/watch?v=P_Dd_jRtZrQ&list=PLj-iqJKiL0NZ9mbuwShcGaldjxz3ypUHs&index=3&t=3s&ab_channel=SahilSShah) goes over the different aspects of the Getting_Started_Arduino

Please work through these examples to get familiar with the board. 
